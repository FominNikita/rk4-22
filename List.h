#include "People.h"
#include "People.h"
#include <iostream>
#include <string.h>
#include <string>
class List
{
public:
	struct Data
	{
		Data *next;
		People *people;
		
		Data(People *people, Data* next)
		{
			
			this->people = people;
			this->next = next;
		}
	};
	Data *head = NULL;
	char fname[MAX_LINE] = "default.bin";
	List();
	~List();
	void Iterate();
	void NotShowSex(std::string sex);
	void NotShowBirthDay(std::string birth_day);
	void NotShowPosition(std::string Position);
	void ChangeAllToShow();
	Data* FindData(int target);
	void Change(int target, std::string name, std::string sur_name, std::string position, std::string sex, std::string birth_day, double salary);
	void AddAtBeginning(People* people);
	void AddAtEnd(People *people);
	void InsertCell(Data* after_me, People *people);
	void DeleteAfter(Data *after_me);
	void DeleteAll();
	void Sort(int key);

	//------------------------------------------------------------------------------------------------------------------------------
	void SaveFile();
	void LoadFile();
	void CreateTestDB();
	void ClearDB();
	void PrintDB();

	People* FindID(int id); //Находит запись с номером id

};